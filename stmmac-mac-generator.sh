#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-2.0

# Grab the board serial number given by androidboot.serialno in /proc/cmdline
# and use that to generate a MAC address that will be stable for this
# particular system.

set -e

OUI="8a:12:4e" # The OUI is Qualcomm, with the U/L bit set to local

__SN="$(</proc/cmdline)"
if [[ "$__SN" == *"androidboot.serialno="* ]]
then
    _SN="${__SN##*androidboot.serialno=}"
    SN="${_SN%% *}"
fi

# If you add to this list it _must_ be at the end, since the index
# for each MMIO address listed is used to generate the NEW_MAC
INTERFACES=(20000 23000000 23040000)

mkdir -p /run/systemd/network/

for i in "${!INTERFACES[@]}"; do
    INTERFACE=${INTERFACES[$i]}
    LINK_FILE="10-stmmac-1gb-${INTERFACE}.link"
    NIC=""

    if [[ -n "$SN" ]]
    then
        NIC=$SN
    elif [[ -r /etc/machine-id ]] # Use machine-id as fallback if serial number is not present
    then
        NIC=$(cat /etc/machine-id)
    else # Revert to random MAC attribution
        exit 0
    fi

    NIC="$(printf "%06x" "$(((16#${NIC:0:6}+i) % 16#1000000))")"
    NEW_MAC="${OUI}:${NIC:0:2}:${NIC:2:2}:${NIC:4:2}"

    cat > "/run/systemd/network/${LINK_FILE}" << __EOF__
[Match]
Path=platform-${INTERFACE}*

[Link]
MACAddress=${NEW_MAC}
NamePolicy=keep kernel database onboard slot path
AlternativeNamesPolicy=database onboard slot path
MACAddressPolicy=none
__EOF__

    echo "Created ${LINK_FILE} file with MAC address ${NEW_MAC}"
done
